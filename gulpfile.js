﻿var gulp = require("gulp"),
    del = require("del"),
    concat = require("gulp-concat"),
    cssmin = require("gulp-cssmin"),
    sass = require('gulp-sass'),
    less = require('gulp-less'),
    ts = require('gulp-typescript'),
    notify = require('gulp-notify'),
    // notify = {onError: function(err) { return function(){console.log(err)} }},
    sm = require('gulp-sourcemaps'),
    uglify = require("gulp-uglify"),
    browserSync = require('browser-sync').create(),
    changed = require('gulp-changed'),
    proxyMiddleware = require('http-proxy-middleware'),
    angularTemplateCache = require('gulp-angular-templatecache'),
    addStream = require('add-stream'),
    replace = require('gulp-replace'),
    htmlReplace = require('gulp-html-replace'),
    htmlmin = require('gulp-htmlmin'),
    hash = require('gulp-hash-filename'),
    merge2 = require('merge2');


var paths = {
    webroot: "./src/"
};

var tsProject = ts.createProject('./src/tsconfig.json', { noExternalResolve: false });


// paths.js = paths.webroot + "js/**/*.js";
paths.ts = paths.webroot + "app/**/*.ts";
paths.test = paths.webroot + "app/**/*.spec.ts";
paths.sass = paths.webroot + "sass/**/*.scss";
paths.less = paths.webroot + "less/**/*.less";
paths.minJs = paths.webroot + "js/**/*.min.js";
paths.css = paths.webroot + "css/**/*.css";
paths.minCss = paths.webroot + "css/**/*.min.css";
paths.cssDest = paths.webroot + "css";
paths.jsDest = paths.webroot + "app";
paths.concatJsDest = paths.webroot + "app/**/*.js";
paths.concatCssDest = paths.webroot + "css/site.min.css";
paths.html = paths.webroot + 'views/**/*.html';


gulp.task('sass', function() {
    gulp.src(paths.sass)
        // .pipe(changed(paths.cssDest, { extension: '.css' }))
        .pipe(sm.init())
        .pipe(sass({
            errLogToConsole: false,
            outputStyle: 'compact',
            includePaths: require("bourbon").includePaths,
        }))
        .on("error", notify.onError("sass: <%= error.message %>"))
        .pipe(sm.write('./', {
            includeContent: false,
            sourceRoot: '../sass'
        }))
        .pipe(gulp.dest(paths.cssDest))
        .pipe(browserSync.stream({ match: '**/*.css' }));
});

gulp.task('less', function() {
    gulp.src(paths.less)
        .pipe(changed(paths.cssDest, { extension: '.css' }))
        .pipe(sm.init())
        .pipe(less({ errLogToConsole: true }))
        .on("error", notify.onError("less: <%= error.message %>"))
        .pipe(sm.write('./', {
            includeContent: false,
        }))
        .pipe(gulp.dest(paths.cssDest))
        .pipe(browserSync.stream({ match: '**/*.css' }));
});

gulp.task('typescript', function() {
    gulp.src(['./src/typing.d.ts', paths.ts, '!' + paths.test], { base: paths.webroot + 'app' })
        .pipe(changed(paths.jsDest, { extension: '.js' }))
        .pipe(sm.init())
        .pipe(ts(tsProject))
        .on("error", notify.onError("tsc: <%= error.message %>"))
        .pipe(sm.write('./', {
            includeContent: false,
            sourceRoot: '/app/'
        }))
        .pipe(gulp.dest(paths.jsDest));

});

gulp.task('serve', ['typescript', 'sass', 'less'], function() {

    var proxies = [
        {
            source: '/api',
            target: 'http://taadod.app:8000/'
        }
    ];

    browserSync.init({
        open: false,
        server: {
            baseDir: paths.webroot,
            middleware: proxies.map(function(x) {
                return proxyMiddleware(x.source, {
                    target: x.target,
                    logLevel: 'debug',
                    changeOrigin: true   // for vhosted sites, changes host header to match to target's host
                });
            })
        }
    });

    gulp.watch(paths.sass, ['sass']);
    // gulp.watch(paths.less, ['less']);
    gulp.watch([paths.ts, '!' + paths.test], ['typescript']).on('change', browserSync.reload);
    gulp.watch(paths.html).on('change', browserSync.reload);

});

/**
 * 
 * Build tasks
 * 
 */

gulp.task('clean', function() {
    return del([
        './dist/**'
    ], {
            force: true
        });
});

gulp.task('build:app:systemjs', function() {

    var Builder = require('systemjs-builder');
    var builder = new Builder('./src', './src/config.js');

    return builder
        .buildStatic('app/app.js', './dist/app.js', {
            minify: false,
            sourceMaps: false,
            lowResSourceMaps: false,
            runtime: false
        })
        .then(function() {
            console.log('build completed')
        })
        .catch(function(err) {
            console.log('Build error');
            console.log(err);
        });

});

gulp.task('build:app', ['build:app:systemjs'], function() {

    function prepareTemplates() {
        return gulp.src('./src/views/**/*.html')
            .pipe(htmlmin({
                collapseWhitespace: true,
                removeAttributeQuotes: true,
                removeComments: true
            }))
            .pipe(angularTemplateCache({
                root: 'views'
            }));
    }

    return merge2(gulp
        .src('./dist/app.js')
        .pipe(uglify({
            compress: {
                drop_console: true
            }
        }))
        .pipe(addStream.obj(prepareTemplates()))
        .pipe(concat('app.js'))
        .pipe(hash())
        .pipe(gulp.dest('./dist')),
        del('./dist/app.js'));
});

gulp.task('build:lib', function() {
    var libs = [
        'bower_components/jquery/dist/jquery.js',
        'lib/lodash.js',
        'bower_components/moment/moment.js',
        'lib/ArrayHelper.js',
        'bower_components/angular/angular.js',
        'bower_components/angular-animate/angular-animate.js',
        'bower_components/angular-aria/angular-aria.js',
        'bower_components/angular-messages/angular-messages.js',
        'lib/loading-bar/loading-bar.js',
        'lib/amPersistance/amStorage.js',
        'lib/amPersistance/amCache.js',
        'bower_components/pleasejs/dist/Please.js',
        'bower_components/angular-ui-router/release/angular-ui-router.js',
        'bower_components/angular-material/angular-material.js',
    ];



    return gulp.src(libs.map(x => `src/${x}`))
        .pipe(concat('lib.js'))
        .pipe(uglify())
        .on("error", notify.onError("<%= error.message %>"))
        .pipe(hash())
        .pipe(gulp.dest('dist'));

});

gulp.task('build:css', function() {
    var lib = [
        'lib/material-icons-local/material-icons.css',
        'bower_components/angular-material/angular-material.css',
        'lib/loading-bar/loading-bar.css',
        'css/roboto.css',
        'css/app.css',
    ].map(x => `src/${x}`);

    return gulp.src(lib)
        // replace the images path since in production builds, css files are in the root folder
        .pipe(replace('../images/', 'images/'))
        .pipe(cssmin({
            keepSpecialComments: 0
        }))
        .pipe(concat('app.css'))
        .pipe(hash())
        .pipe(gulp.dest('./dist'));
});

gulp.task('build:html', ['build:lib', 'build:app', 'build:css'], function(cb) {

    var glob = require('glob');

    // Determine files name
    glob('./dist/*.js', function(err, jsFiles) {

        if (err) {
            cb(err);
            return;
        }

        glob('./dist/*.css', function(err, cssFiles) {
            if (err) {
                cb(err);
                return;
            }

            console.log(jsFiles, cssFiles)

            var replaceDir = x => x.replace('./dist/', '');

            var appJs = jsFiles.map(replaceDir).filter(x => x.startsWith('app-'))[0];
            var libJs = jsFiles.map(replaceDir).filter(x => x.startsWith('lib-'))[0];
            var appCss = cssFiles.map(replaceDir).filter(x => x.startsWith('app-'))[0];

            var config = {
                baseUrl: '/',
                noCache: false,
                cacheDuration: 0.3,
                cacheProvider: 'memory',
                appName: 'ul.mutualfund'
            };

            gulp.src('src/index.html')
                .pipe(htmlReplace({
                    'config': `                        
                        <script>                            
                            window.Config = ${JSON.stringify(config)}
                        </script>
                    `,
                    'css': [appCss],
                    'js': [libJs, appJs, '<script>angular.bootstrap(document.body, [Config.appName])</script>']
                }))
                .pipe(htmlmin({
                    collapseWhitespace: true,
                    removeAttributeQuotes: true,
                    removeComments: true
                }))
                .pipe(gulp.dest('dist/'));

            cb();
        })
    });
});

// copy images and font assets
gulp.task('build:other', function() {
    gulp.src([
        './src/images/**/*',
        './src/fonts/**/*',
        './src/lib/material-icons-local/**/*',
    ], { base: './src' })
        .pipe(gulp.dest('./dist'));
});

gulp.task('build:serve', function() {

    var proxies = [
        {
            source: '/api',
            target: 'http://taadod.app:8000/'
        }
    ];


    browserSync.init({
        open: false,
        server: {
            baseDir: './dist',
            middleware: proxies.map(function(x) {
                return proxyMiddleware(x.source, {
                    target: x.target,
                    logLevel: 'debug',
                    changeOrigin: false,   // for vhosted sites, changes host header to match to target's host
                    onProxyRes: function onProxyRes(proxyRes, req, res) {
                        proxyRes.headers['Connection'] = 'Keep-Alive';
                    }
                });
            })
        }
    });
})

gulp.task('build', ['build:html', 'build:other']);

gulp.task('start', ['serve'])