#!/bin/bash

echo "[build] clean";
gulp clean;

echo "[build] start";
gulp build;
echo "[build] complete";

echo "[build] compressing";
tar -czf dist.gz dist
echo "[build] done";

