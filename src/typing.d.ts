// lodash
declare var _:any;
declare module "lodash" {
    export = _;
}

// jquery
declare var $:any;
declare var jQuery:any;
declare module "jquery" {
    export = $;
}

// angular
declare var angular:any;
declare module "angular" {
    export = angular;
}

// moment
declare var moment:any;
declare module "moment" {
    export = moment;
}

// Please
declare var Please:any;
declare module "Please" {
    export = Please;
}

// Snap
declare var Snap:any;
declare var mina:any;
declare module "Snap" {
    export = Snap;
}


// math
declare var math:any;
declare module "math" {
    export = math;
}