;(function (angular, window) {



    function BaseStorageEngine(engine) {

        this.get = function (key) {
            try {
                return angular.fromJson(engine.getItem(key) || null);
            } catch (ex) {
                return null;
            }
        };

        this.set = function (key, value) {
            engine.setItem(key, angular.toJson(value));
        };

        this.remove = function (key) {
            engine.removeItem(key);
        };

        this.clear = function () {
            engine.clear();
        };

        this.keys = function () {
            return typeof engine.keys === 'function' ? engine.keys() : Object.keys(engine);
        };
    };

    function MemoryStorage() {
        var storage = {};

        this.getItem = function (key) {
            return storage[key] || null;
        };

        this.setItem = function (key, value) {
            storage[key] = value;
        };

        this.removeItem = function (key) {
            delete storage[key];
        };

        this.keys = function () {
            return Object.keys(storage);
        }

        this.clear = function () {
            storage = {};
        };
    }

    var localStorage = window.localStorage,
        sessionStorage = window.sessionStorage,
        memoryStorage = MemoryStorage;



    function LocalStorageEngine() {
        BaseStorageEngine.call(this, localStorage);
    };

    LocalStorageEngine.prototype = new BaseStorageEngine();
    LocalStorageEngine.prototype.constructor = LocalStorageEngine;

    function SessionStorageEngine() {
        BaseStorageEngine.call(this, sessionStorage);
    };

    SessionStorageEngine.prototype = new BaseStorageEngine();
    SessionStorageEngine.prototype.constructor = SessionStorageEngine;


    function MemoryStorageEngine() {
        BaseStorageEngine.call(this, new memoryStorage);
    };

    MemoryStorageEngine.prototype = new BaseStorageEngine();
    MemoryStorageEngine.prototype.constructor = MemoryStorageEngine;



    var engines = {
        'memory': MemoryStorageEngine,
        'local': LocalStorageEngine,
        'session': SessionStorageEngine,
    };


    var app = angular.module('amStorage', []);

    app.provider('amStorage', function amStorageProvider() {

        var defaultEngine = 'memory';

        this.getEngine = function () {
            return defaultEngine;
        };

        this.setEngine = function (engine) {

            if (Object.keys(engines).indexOf(engine) === -1) {
                throw "Engine " + engine + " not supported\n available engines: " + Object.keys(engines).join(', ');
            }

            defaultEngine = engine;
        };

        this.addEngine = function (name, engine) {
            engines[name] = engine;
        };

        this.removeEngine = function (name) {
            delete engines[name];
        };

        this.$get = [function amStorageFactory() {
            var engine = engines[this.getEngine()];
            return new engine();
        }];


    });

    angular.forEach(engines, function (engine, name) {
        var qualifiedName = name[0].toUpperCase() + name.slice(1);
        app.service('am' + qualifiedName + 'Storage', engine);
    });


})(angular, window);
