;(function (angular, window) {

    var app = angular.module('amCache', ['amStorage']);

    app.provider('amCache', [function amCacheProvider() {

        var options = {
            prefix: '__',
            duration: 60
        };

        this.option = function () {
            if (angular.isString(arguments[0])) {
                if (angular.isDefined(arguments[1])) {
                    options[arguments[0]] = arguments[1];
                } else {
                    return arguments[0];
                }
            } else if (angular.isObject(arguments[0])) {
                angular.extend(options, arguments[0]);
            }
        };

        var StrHelper = {
            startsWith: function (str, search) {
                return str.indexOf(search) === 0;
            },

            endsWith: function (str, search) {
                return str.indexOf(search, str.length - search.length) !== -1;
            },

            contains: function (str, search) {
                return str.indexOf(search) !== -1;
            }
        };



        function Cache(storageEngine) {

            var storage = storageEngine;

            this.setStorage = function (storageEngine) {
                storage = storageEngine;
            };

            /**
             * Store a key, value pair in the cache
             * 
             * @param string key
             * @param object value
             * @param int duration duration in seconds
             */
            this.put = function (key, value, duration) {
                if (angular.isUndefined(value)) return;

                var now = +(new Date);
                var duration = parseInt(duration) || options.duration;

                storage.set(options.prefix + key, {
                    value: value,
                    expiry: now + duration * 1000
                });

                return value;
            };


            this.get = function (key, _default) {
                
                var k = options.prefix + key;
                var row = storage.get(k);
                var now = +(new Date);

                if (row) {

                 
                    if (row.expiry > now) {
                        return row.value;
                    }


                    storage.remove(k);
                }
               
                return _default;
            };

            this.keys = function () {
                return storage.keys().filter(function (r) {
                    return r.indexOf(options.prefix) === 0;
                }).map(function (r) {
                    return r.slice(options.prefix.length);
                });
            };


            this.remove = function (key) {
                storage.remove(options.prefix + key);
            };

            this.removeWhere = function (whereDelegate) {
                var self = this;
                this.keys().forEach(function (key) {
                    if (whereDelegate(key, self)) {
                        self.remove(key);
                    }
                });
            };

            this.removeContains = function (str) {
                this.removeWhere(function (key) {
                    return StrHelper.contains(key, str);
                });
            };

            this.removeStarts = function (str) {
                this.removeWhere(function (key) {
                    return StrHelper.startsWith(key, str);
                });
            };

            this.removeEnds = function (str) {
                this.removeWhere(function (key) {
                    return StrHelper.endsWith(key, str);
                });
            };

            this.removeAll = function () {
                storage.clear();
            };

            this.destroy = function () {
                storage.clear();
            };

            this.info = function () {
                return {}; // not implemented 
            };


        };


        this.$get = ['amStorage', function amCacheFactory(amStorage) {
            return new Cache(amStorage);
        }];

    }]);

})(angular, window);
