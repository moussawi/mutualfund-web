/**
 * Array helper
 * 
 * @author Ahmad Moussawi <ahmad.moussawi@live.com>
 * 
 */

var ArrayHelper = {

    /**
     * Remove the first occurrence 
     * 
     * @param Array
     * @param function
     * @returns the removed item
     *
     */
    remove: function(array, predict) {
        for (var i = 0; i < array.length; i++) {
            if (predict(array[i])) {
                return array.splice(i, 1);
            }
        }
    },

    /**
     * Remove all occurrences
     * 
     * @param Array
     * @param function
     * @returns the removed items
     *
     */
    removeAll: function(array, predict) {
        var removed = [];
        for (var i = 0; i < array.length; ) {
            if (predict(array[i])) {
                removed.push(array.splice(i, 1));
            }else{
                i++;
            }
        }

        return removed;
    }
};