export class NumberHelper {
    static humanBytes(bytes) {
        var s = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'];
        var e = Math.floor(Math.log(bytes) / Math.log(1024));
        return (bytes / Math.pow(1024, e)).toFixed(2) + " " + s[e];
    }

    static human(num, precision = 0): any {
        num = Number(num);
        if (num === 0) return 0;

        var s = ['', 'k', 'M', 'G', 'T', 'P'];
        var scale = 1000;
        var e = Math.floor(Math.log(Math.abs(num)) / Math.log(scale));
        return (num / Math.pow(scale, e)).toFixed(precision) + " " + s[e];
    }
}

export class Util {

    static and(...args : any[]) {        
        for (var i = 0; i < args.length; i++) {
            if (!args[i]) {
                return false;
            }
        }

        return true;
    }

    static or(...args : any[]) {        
        for (var i = 0; i < args.length; i++) {
            if (!!args[i]) {
                return true;
            }
        }
        return false;
    }

    static guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }


    static slug(str:string) {
        return str
            .toLowerCase()
            .replace(/[^\w ]+/g, '')
            .replace(/ +/g, '-');
    }
    
    static pluralize(count: number, oneSuffix: string, otherSuffix: string, zeroSuffix: string = otherSuffix){
        
        otherSuffix = otherSuffix.replace('{}', count.toString());
        
        switch(count) {
            case 1: 
                return oneSuffix;
            case 0:
                return zeroSuffix;
            default:
                return otherSuffix;
        }
    }
}
