import dep from './boot/dep';
import routes from './boot/routes';
import controllers from './boot/controllers';
import services from './boot/services';
import directives from './boot/directives';
import filters from './boot/filters';
import LoadingStatus from './LoadingStatus';
import View from './View';

// Configuration defaults
window['Config'].baseUrl = _.get(window, 'Config.baseUrl', '/');
window['Config'].legacyAppUrl = _.get(window, 'Config.legacyAppUrl', window['Config'].baseUrl);
window['Config'].apiUrl = _.get(window, 'Config.apiUrl', window['Config'].baseUrl + 'api/');


var app = angular.module(window['Config'].appName, dep);
var templateModule = angular.module('templates', []); // used for caching html templates

app.config([
    '$stateProvider',
    '$urlRouterProvider',
    '$locationProvider',
    '$mdThemingProvider',
    '$mdIconProvider',
    'cfpLoadingBarProvider',
    'amStorageProvider',
    'amCacheProvider',
    '$httpProvider',
    (
        $stateProvider,
        $urlRouterProvider,
        $locationProvider,
        $mdThemingProvider,
        $mdIconProvider,
        cfpLoadingBarProvider,
        amStorageProvider,
        amCacheProvider,
        $httpProvider) => {
                   
        $httpProvider.interceptors.push('AuthInterceptor');
        $httpProvider.defaults.cache = true;
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        $httpProvider.defaults.withCredentials = true;

        // $locationProvider.html5Mode(true);

        amStorageProvider.setEngine(_.get(window, 'Config.cacheProvider', 'memory'));

        amCacheProvider.option({
            prefix: '__',
            duration: window['Config'].cacheDuration * 60
        });

        // Routes registration
        angular.forEach(routes, (route, name) => {
            // console.debug(`[route] ${name} => ${route.url}`);
            if (!_.get(route, 'data.title')) {
                route.data = { title: '' };
            }

            $stateProvider.state(name, route);
        });

        $urlRouterProvider.otherwise('/');

        $mdThemingProvider.theme('default')
            .primaryPalette('blue');

        // loading bar
        cfpLoadingBarProvider.includeSpinner = false;
        cfpLoadingBarProvider.loadingBarTemplate = '<loader><div class="loader">...</div></loader>';


    }]);

app.run(['$state', '$rootScope', '$http', 'amStorage', 'amCache', ($state, $rootScope: any, $http, amStorage, amCache) => {
    // check for Authentication

    $rootScope._ = _;
    $rootScope.$state = $state;
    $rootScope.LoadingStatus = LoadingStatus;
    $rootScope.View = View;
    $rootScope.windowHeight = window.outerHeight;
        

    // store the current state;
    var currentState;
    $rootScope.isState = (state) => currentState === state;

    $rootScope.$on('$stateChangeSuccess', (event, toState, toParams, fromState, fromParams) => {
        currentState = toState;
        var $body = $('body');
        var prefix = 'state';
        var currentClass = `${prefix}-${toState.name.replace(/\./g, '-')}`;

        var classList = $body
            .attr('class')
            .split(/\s+/g)
            .filter(x => x.indexOf(`${prefix}-`) === -1);

        classList.push(currentClass);

        $('body').attr('class', classList.join(' '));

        // handle default sub state
        if (toState.redirectTo) {
            event.preventDefault();
            $state.go(toState.redirectTo, toParams)
        }

    });

    setTimeout(function() {
        $('#bootstrapIndicator').fadeOut(500, () => {
            $('#bootstrapIndicator').remove();
        });
    }, 100)


}])

// Controllers registration
angular.forEach(controllers, (type: any) => {
    var name = type.$name || type.name;
    // console.debug(`[controller] ${name}`);
    app.controller(name, type);
});

// Directives registration
angular.forEach(directives, (type: any) => {
    var name = type.$name || type.name;
    // console.debug(`[directive] ${name}`);
    app.directive(type.$name, type);
});

// Services registration
angular.forEach(services, (type: any) => {
    var name = type.$name || type.name;
    // console.debug(`[service] ${name}`);

    if (type.factory) {
        // console.debug('register via factory ' + name);
        app.factory(name, type.factory());
    } else if (type.$registerAs === 'factory') {
        app.factory(name, type);
    } else {
        app.service(name, type);
    }

});

// Filters registration
angular.forEach(filters, (filter, name) => {
    // console.debug(`[filter] ${name}`);
    app.filter(name, filter);
});

