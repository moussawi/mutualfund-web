enum LoadingStatus{
	Initial,
	Loading,
	Ready,
	NoData
}

export default LoadingStatus; 