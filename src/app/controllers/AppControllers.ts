import {AuthService} from '../services/Auth';
import {Profile} from '../services/Profile';
import {Util} from '../Helper';

export class AppController {

    user: any = null;
    roles: string[] = [];
    loaded = false;
    authenticated = true;

    menu: any = [
        {
            state: 'root',
            title: 'الرئيسية',
            icon: 'home',
            active: true,
            hideOnHome: true
        },
        {
            state: 'beneficiaries',
            title: 'المستفيدين',
            icon: 'account_circle'
        },
        {
            state: 'financialaid',
            title: 'المنح التعليمية',
            icon: 'thumb_up'
        },
        {
            state: 'medicalaid',
            title: 'المساعدات المرضية',
            icon: 'accessibility'
        },
        {
            state: 'settings',
            title: 'الاعدادت',
            icon: 'settings'
        },
    ];

    static $inject = [
        '$scope',
        '$mdSidenav',
        '$state',
        '$rootScope',
        'cfpLoadingBar',
        'AuthService',
        'Profile',
        'amSessionStorage',
        '$timeout'
    ];

    static $name = 'AppController';

    constructor(
        private $scope: any,
        private $mdSidenav,
        private $state,
        private $rootScope,
        private cfpLoadingBar: any,
        private AuthService: AuthService,
        private Profile: Profile,
        private amSessionStorage,
        $timeout
    ) {

        // hide the app in case of 401
        $scope.$on('error:401', () => {
            this.authenticated = false;
        });
        
        $scope.$on('home:loaded', (event, data) => {            
            this.menu[1].notification = data.beneficiaries.length;    
            this.menu[2].notification = data.financial_aid.length;    
            this.menu[3].notification = data.medical_aid.length;
        });
        
                
        
        this.AuthService.info().then((info: any) => {


            this.loaded = true;

            this.user = info.user;
            this.roles = info.roles;

            this.menu = this.menu.filter(x =>
                (_.isUndefined(x.roles) || _.intersection(this.roles, x.roles).length) &&
                (_.isUndefined(x.enabled) || _.isBoolean(x.enabled) && x.enabled === true || _.isFunction(x.enabled) && x.enabled() === true)
            );
        })

    }

    showApp() {
        return this.loaded || !this.authenticated;
    }

    hasRole(role) {
        return this.roles.indexOf(role) > -1;
    }

    toggleSideNav() {
        this.$mdSidenav('left').toggle();
    }

    closeSideNav() {
        this.$mdSidenav('left').close();
    }
}

export class SettingsController {

    settings: any = [
        /*
        {
            icon: 'stay_current_portrait',
            value: true,
            name: 'Force using mobile version',
            onChange: ($event, item) => {

            }
        },
        */        
    ];

    static $inject = ['$scope', '$state', 'AuthService', '$mdToast'];
    static $name = 'SettingsController';

    constructor(protected $scope, protected $state, protected AuthService: AuthService, protected $mdToast) {
       
    }


    logout() {

        if (!confirm('Are you sure you want to logout ?')) {
            return;
        }

        return this.AuthService.logout().then(x => {
            this.$state.go('auth.login');
            this.$mdToast.showSimple('Logged out succesfully');
        });
    }

    clearCache() {
        this.AuthService.clearCache();
        this.$mdToast.showSimple('Cache cleared');
    }


}