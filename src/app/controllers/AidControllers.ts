import { Aid } from '../services/Aid';

export class FinancialAidController {

    rows: any[];
    sum = 0;

    static $name = 'FinancialAidController';
    static $inject = [
        'Aid'
    ];

    constructor(
        protected Aid: Aid
    ) {

        this.Aid.financialQuery().then(q => {
            this.rows = q.value();

            for (var i = 0; i < this.rows.length; i++) {
                this.sum += this.rows[i].SumHoussatSandouk;
            };

        });
    }
}

export class MedicalAidController {

    rows: any[];
    sum = 0;

    static $name = 'MedicalAidController';
    static $inject = [
        'Aid'
    ];

    constructor(
        protected Aid: Aid
    ) {

        this.Aid.medicalQuery().then(q => {

            this.rows = q.value();

            this.sum = this.rows.map(x => Number(x.SumHoussatSandouk)).reduce((a, b) => {
                return a + b;
            }, 0) * 1000;
            
        });

    }
}
