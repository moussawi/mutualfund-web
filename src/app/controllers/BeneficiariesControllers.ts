import {Beneficiaries} from '../services/Beneficiaries';
import {NumberHelper} from '../helper';

export class BeneficiariesController {

    rows: any[];

    static $name = 'BeneficiariesController';
    static $inject = [
        'Beneficiaries'
    ];

    constructor(
        protected Beneficiaries: Beneficiaries
    ) {

        this.Beneficiaries.allQuery().then(q => {
            this.rows = q.value();
        });
    }
}


export class BeneficiariesDetailsController {

    pharmacyTreatments: any[];
    labTreatments: any[];
    chronicDiseases: any[];
    labLimit = 5;
    pharmLimit = 5;

    static $name = 'BeneficiariesDetailsController';
    static $inject = ['$scope', 'Beneficiaries', '$stateParams'];

    constructor($scope, protected Beneficiaries: Beneficiaries, protected $stateParams) {
        $scope.visible = {
            'lab': {
                '0': true
            },
            'pharm': {
                '0': true
            }
        };

        $scope.toggle = (type, index) => {
            if (!$scope.visible[type][index]) {
                $scope.visible[type][index] = true;
            } else {
                $scope.visible[type][index] = false;
            }
        };

        $scope.total = (group, prop) => {
            var sum = _(group).sum(x => _.get(x, prop));
            return NumberHelper.human(sum);
        }

        $scope.summary = (group, prop) => {
            var pharms = _(group)
                .map(x => _.get(x, prop))
                .uniq()
                .value();
            return `${pharms.join(', ')}`
        }

        this.loadPharm(this.pharmLimit);
        this.loadLab(this.labLimit);

        Beneficiaries.chronicDiseasesQuery($stateParams.id).then(q => {
            this.chronicDiseases = q.value();
            console.log(this.chronicDiseases);
        });

    }

    loadPharm(limit) {
        this.Beneficiaries.pharmacyTreatmentsQuery(this.$stateParams.pid).then(q => {
            q = q.groupBy(x => moment(x.ref_date_reel).format('MMM, YYYY'))
                .map((v, k) => {
                    return {
                        label: k,
                        values: v
                    }
                });

            q = limit ? q.take(limit) : q;
            this.pharmacyTreatments = q.value();
        });
    }

    loadLab(limit) {
        this.Beneficiaries.labTreatmentsQuery(this.$stateParams.pid).then(q => {
            q = q.groupBy(x => moment(x.VCH_DATE).format('YYYY'))
                .map((v, k) => {
                    return {
                        label: k,
                        values: v
                    }
                })
                .sortBy(x => -x.label);

            q = limit ? q.take(limit) : q;
            this.labTreatments = q.value();
        });
    }

    showAllPharm() {
        this.pharmLimit = 0;
        this.loadPharm(this.pharmLimit);
    }

    showAllLab() {
        this.labLimit = 0;
        this.loadLab(this.labLimit);
    }



}