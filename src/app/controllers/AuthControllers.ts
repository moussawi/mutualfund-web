import {AuthService} from '../services/Auth';

export class AuthController {

    private model: any = {
        username: '1410',
        password: '6751'
    };

    static $name = 'AuthController';
    static $inject = ['$scope', '$state', 'AuthService', '$mdToast'];
    constructor(
        private $scope: any,
        private $state,
        private AuthService: AuthService,
        private $mdToast: any
    ) {

    }

    login() {
        this.AuthService.login(this.model)
            .then((r: any) => {
                this.$state.go('root');
            }).catch((err: any) => {
                this.$mdToast.showSimple('Login failed, please try again later');
            });
    }

}