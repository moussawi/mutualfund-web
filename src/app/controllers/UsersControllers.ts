import {Users} from '../services/Users';
import View from '../View';

export class UsersController {

    rows: any[];

    static $name = 'UsersController';
    static $inject = [
        '$scope'       
        , 'Users'
        , 'Session'
        , '$mdDialog'       
        , '$mdToast'
        , '$state'
        , '$q'
        , '$stateParams'
        , 'amLocalStorage'
    ];

    constructor(
        protected $scope,
        protected Users: Users,
        protected Session,
        protected $mdDialog,
        protected $mdSidenav,
        protected $mdToast,
        protected $state,
        protected $q,
        protected $stateParams,
        protected amLocalStorage) {

        this.Users.allQuery().then(q => {
            this.rows = q.value();
            console.log(this.rows[0]);
        });
    }

    openEditDialog(row?) {
        var data = [];

        if (row) {
            data.push(row);
        }

        this.$mdDialog.show({
            controller: EditDialogController,
            templateUrl: View('users.edit'),
            parent: angular.element(document.body),
            targetEvent: null,
            clickOutsideToClose: true,
            locals: {
                data
            }
        }).then((r) => {

            if (r.action === 'new') {
                this.rows.unshift(r.row);
            } else if (r.action === 'remove') {
                this.rows = _.filter(this.rows, x => x.id !== r.row.id);
            } else if (r.action === 'update') {
                var row = _(this.rows).filter(x => x.id === r.row.id).first();
                _.assign(row, r.row);
            }

        }, () => {

        });
    }


}

class EditDialogController {

    static $inject = ['$scope', '$mdDialog', 'Session', 'Users', '$mdToast', 'data'];
    constructor(protected $scope, protected $mdDialog, Session, protected Users: Users, $mdToast, data) {

        $scope.isEdit = false;

        $scope.viewData = {};

        $scope.model = {
            name: void 0,
            description: void 0
        };

        if (data[0]) {
            _.assign($scope.model, data[0]);
            $scope.isEdit = true;
        }

        console.log(data);

        /**
         * Create the entity
         */
        $scope.save = () => {
            Users.save($scope.model).then(r => {

                if ($scope.isEdit) {
                    $mdDialog.hide({
                        action: 'update',
                        row: r,
                    });
                } else {
                    $mdDialog.hide({
                        action: 'new',
                        row: r,
                    });
                }
            }).catch(err => {
                alert('Failed to create a new user');               
            });
        };

        $scope.remove = () => {
            if (!confirm('Are you sure you want to delete ?')) {
                return;
            }

            this.Users.remove($scope.model.id).then(r => {
                this.$mdDialog.hide({
                    action: 'remove',
                    row: r
                });
            })


        }

        /**
         * Close the dialog
         */
        $scope.cancel = function() {
            $mdDialog.cancel();
        };
    }

}