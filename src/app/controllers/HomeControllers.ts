import {Profile} from '../services/Profile';

export class HomeController {

	private viewBag: any;
	
	static $inject = ['$rootScope', '$scope', '$state', 'Profile'];
	static $name = 'HomeController';
	
	constructor($rootScope, private $scope: any, private $state, private Profile: Profile) {
		Profile.profile().then(data => {
            $scope.profile = data;            
            $rootScope.$broadcast('home:loaded', data);            
        })
	}  
}   