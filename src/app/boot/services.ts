import {Backend} from '../services/Backend';
import * as Auth from '../services/Auth';
import {Session} from '../services/Session';
import * as Util from '../services/Util';
import * as Users from '../services/Users';
import * as Beneficiaries from '../services/Beneficiaries';
import * as Aid from '../services/Aid';
import * as Profile from '../services/Profile';

export default [
	Backend,
    Util.Util,
	Auth.AuthService,
	Auth.AuthInterceptor,
	Session,
    Beneficiaries.Beneficiaries,
    Aid.Aid,
    Profile.Profile,
];