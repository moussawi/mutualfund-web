/**
 * Here put the app dependencies
 * 
 */
var dep = [
	'ngAnimate',
	'ui.router',
	'ngMaterial',
	'ngMessages',	
	'angular-loading-bar',
	'cfp.loadingBar', // custom api
	'amStorage',
	'amCache',	
    'templates',    
];

export default dep;