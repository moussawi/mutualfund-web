import * as App from '../controllers/AppControllers';
import * as Home from '../controllers/HomeControllers';
import * as Auth from '../controllers/AuthControllers';
import * as Users from '../controllers/UsersControllers';
import * as Beneficiaries from '../controllers/BeneficiariesControllers';
import * as Aid from '../controllers/AidControllers';



export default [       
	App.AppController,  
    App.SettingsController, 
	Home.HomeController,
	Auth.AuthController,	    
    Beneficiaries.BeneficiariesController,
    Beneficiaries.BeneficiariesDetailsController,
    Aid.FinancialAidController,
    Aid.MedicalAidController,
];



