import { NumberHelper } from '../helper';

var isLocalhost = window.location.hostname.indexOf('localhost') > -1;

var parse, parseUtc;

if (isLocalhost) {
    parse = i => moment(i);
    parseUtc = i => moment.utc(i);
} else {
    parse = i => moment(i, 'MMM DD YYYY HH:mm:ss');
    parseUtc = i => moment.utc(i, 'MMM DD YYYY HH:mm:ss');
}


var filters = {
    'human': () => (input) => NumberHelper.human(input),
    'ago': () => (input, withoutSuffix = false) => parse(input).fromNow(withoutSuffix),
    'agoutc': () => (input) => parseUtc(input).fromNow(),
    'moment': () => (input, format: string) => parse(input).format(format),
    'momentutc': () => (input, format: string) => parseUtc(input).local().format(format),
    'end': () => (input, last = 4) => ('' + input).slice(-last),
}


export default filters; 