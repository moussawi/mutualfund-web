import View from '../view';

interface RouteCollection {
    [index: string]: any
}

var routes: RouteCollection = {
    'root': {
        url: '/',
        controller: 'HomeController as ctrl',
        templateUrl: View('home')
    },

    'auth': {
        abstract: true,
        url: '/auth',
        template: '<ui-view></ui-view>'
    },

    'auth.login': {
        url: '/login',
        controller: 'AuthController as ctrl',
        templateUrl: View('auth.login')
    },

    'settings': {
        url: '/settings',
        controller: 'SettingsController as ctrl',
        templateUrl: View('settings')
    },
    
    'users': {
        url: '/users',
        controller: 'UsersController as ctrl',
        templateUrl: View('users')        
    },  
    
    'beneficiaries': {
        url: '/beneficiaries',
        controller: 'BeneficiariesController as ctrl',
        templateUrl: View('beneficiaries')        
    },

    'beneficiaries/details': {
        url: '/beneficiaries/:id/:pid',
        controller: 'BeneficiariesDetailsController as ctrl',
        templateUrl: View('beneficiaries.details')        
    },

    'financialaid': {
        url: '/financialaid',
        controller: 'FinancialAidController as ctrl',
        templateUrl: View('financialaid')        
    },
      
    'medicalaid': {
        url: '/medicalaid',
        controller: 'MedicalAidController as ctrl',
        templateUrl: View('medicalaid')        
    },  
};

export default routes;
