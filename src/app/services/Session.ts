/**
 * In memory Session
 */
export class Session {
	session:any = {};
	
	static $inject = [];
	static $name = 'Session';

	constructor(){
			return this;	 
	}
	
	set(key:string, value:any){
		_.set(this.session, key, value);
	}
	
	has(key){
		return _.has(this.session, key);
	}
	
	get(key, def?){
		return _.get(this.session, key, def);
	}
	
	remove(key){
		delete this.session[key];
	}	
}