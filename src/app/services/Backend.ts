export class Backend {

    protected listPropertyName;

    protected baseUrl = window['Config'].apiUrl;

    static $inject = ['$http', '$q'];
    static $name = 'Backend';
    static $registerAs = 'service';

    constructor(
        protected $http,
        protected $q,
        apiPrefix?: string
    ) {
        if (apiPrefix) {
            this.baseUrl += apiPrefix;
        }
    }

    private asQuery(response) {
        return _(response.data);
    }

    private buildUrl(url: string, params: any = null) {

        if (!params) {
            return this.baseUrl + url;
        }

        var op = url.indexOf('?') === -1 ? '?' : '&';

        return this.baseUrl + url + op + $.param(params);

    }

    protected get(api: string = '', params?: any, config?) {
        return this.$http.get(this.buildUrl(api, params), config)
            .then(this.asQuery)
            .catch(err => this.$q.reject(err.data));
    }

    protected post(api: string = '', params?: any, config?) {
        return this.$http.post(this.buildUrl(api), params, config)
            .then(v => v.data)
            .catch(err => this.$q.reject(err.data));
    }

    protected postAsQuery(api: string, params?: any, config?) {
        return this.$http.post(this.buildUrl(api), params, config)
            .then(this.asQuery)
            .catch(err => this.$q.reject(err.data));
    }

    allQuery() {
        return this.get()
            .then(q => q.thru(v => v[this.listPropertyName]));
    }

    findLocal(id) {
        return this.allQuery()
            .then(q => q.filter(x => x.id == id).first());
    }

    find(id) {
        return this.get(`details/${id}`);
    }
    
    save = (entity) => {
        return this.post('', entity);
    }

    remove(id) {
        return this.post(`delete/${id}`);
    }

}