import { Backend } from './Backend';
import { Util } from './Util';

export class Aid extends Backend {

    protected listPropertyName = 'family';

    static $name = 'Aid';
    static $inject = ['$http', '$q', '$timeout', 'Util'];

    constructor(
        protected $http,
        protected $q,
        protected $timeout,
        protected Util: Util
    ) {
        super($http, $q);
        this.baseUrl += 'teacher/';
    }

    financialQuery() {
        return this.get()
            .then(q => q.thru(x => x.financial_aid))
            .then(q => q.sortBy(x => -moment(_.get(x, 'decree.assignments[0].DateIhala'))))
            .then(q => this.Util.withColors(q, x => x.Karar_n));
    }

    medicalQuery() {
        return this.get()
            .then(q => q.thru(x => x.medical_aid))
            .then(q => q.sortBy(x => -moment(_.get(x, 'decree.assignments[0].DateIhala'))))
            .then(q => this.Util.withColors(q, x => x.Karar_n));
    }
}
