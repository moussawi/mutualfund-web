import {Backend} from './Backend';

export class AuthInterceptor {

    static $inject = ['$rootScope', '$q', '$location'];
    static $name = 'AuthInterceptor';

    static factory() {
        var instance: any = ($rootScope, $q, $location) => {
            return {
                request: (config) => {
                    config.headers = config.headers || {};
                    // here set the headers before sending the request		
                    // config.headers.Authorization = 'myheader value';
                    return config;
                },

                responseError: (rejection) => {
                    if (rejection.status === 401) {
                        console.log('broadcasting 401');
                        $rootScope.$broadcast('error:401');
                        $location.path('/auth/login');
                    }

                    if (rejection.status === 403) {
                        console.log(rejection);
                        $rootScope.$broadcast('error:403');

                        let msg = `You don't have the needed permission
						Not all requests are loaded correctly, if you still face this error
						Contact your administrator
						`.split('\n').map(x => x.trim()).join('\n');

                        alert(msg);
                    }

                    return $q.reject(rejection);
                }
            }
        }

        instance.$inject = AuthInterceptor.$inject;
        return instance;
    }

}

export class AuthService extends Backend {

    protected storageKey = 'user';

    static $inject = ['$http', '$q', 'amSessionStorage'];
    static $name = 'AuthService';

    constructor(protected $http, protected $q, protected amSessionStorage) {
        super($http, $q);
    }

    public login(model) {
        return this.post('auth/login', model).then((r: any) => {

            this.amSessionStorage.set(this.storageKey, r);
            // store the session info in the session here

            return r;
        });
    }

    public info() {

        // here you should implement a real service, that get the roles, and user info

        return this.$q.resolve({
            user: { name: 'Ahmad' },
            roles: ['Admin']
        });
    }

    public logout() {
        return this.post('auth/logout')
            .then(x => {
                this.clearCache();
            });
    }

    public clearCache() {
        this.amSessionStorage.remove(this.storageKey);
    }

}