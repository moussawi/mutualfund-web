import {Backend} from './Backend';
import {Util} from './Util';

export class Beneficiaries extends Backend {

    protected listPropertyName = 'beneficiaries';

    static $name = 'Beneficiaries';
    static $inject = ['$http', '$q', '$timeout', 'Util'];

    constructor(
        protected $http,
        protected $q,
        protected $timeout,
        protected Util: Util
    ) {
        super($http, $q);
        this.baseUrl += 'teacher/';
    }

    allQuery() {
        return super.allQuery()
            .then(q => this.Util.withColors(q, x => x.RelationID))
    }

    pharmacyTreatmentsQuery(beneficiaryId, limit = 0) {
        return this.get()
            .then(q => q.thru(x => x.pharmacy_treatments))
            .then(q => q.filter(x => x.DEPEND == beneficiaryId))
            .then(q => q.sortBy(x => -moment(x.tar_date)))
            .then(q => limit ? q.take(limit) : q)
            .then(q => this.Util.withColors(q, x => x.pharmacy.CODE1))
    }

    labTreatmentsQuery(beneficiaryId, limit = 0) {
        return this.get()
            .then(q => q.thru(x => x.lab_treatments))
            .then(q => q.filter(x => x.DEPEND == beneficiaryId))
            .then(q => q.sortBy(x => -moment(x.VCH_DATE)))
            .then(q => limit ? q.take(limit) : q)
            .then(q => this.Util.withColors(q, x => x.lab.LABNO))
    }

    chronicDiseasesQuery(id) {
        return this.get()
            .then(q => q.thru(x => x.beneficiaries))
            .then(q => q.filter(x => x.Id == id))
            .then(q => q.thru(x => x[0].chronic_diseases))
            .then(q => this.Util.withColors(q, x => x.CodeMalade))            
    }

}
