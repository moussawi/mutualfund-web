import {Backend} from './Backend';
import {Util} from './Util';

export class Users extends Backend {

    /**
     * The property name of the json 
     * 
     * i.e. {
     *   users: [{}, {}]
     * }
     */
    protected listPropertyName = 'users';    

    static $name = 'Users';
    static $inject = ['$http', '$q', '$timeout', 'Util'];

    constructor(
        protected $http,
        protected $q,
        protected $timeout,
        protected Util: Util
    ) {
        super($http, $q);
        this.baseUrl += 'users/';
    }

    allQuery() {
        return super.allQuery()
            .then(q => this.Util.withInitials(q, x => x.fullName))
            .then(q => this.Util.withColors(q, x => x.id));
    }   
}
