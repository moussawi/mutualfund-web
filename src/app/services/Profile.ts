import {Backend} from './Backend';
import {Util} from './Util';

export class Profile extends Backend {

    static $name = 'Profile';
    static $inject = ['$http', '$q', '$timeout', 'Util'];

    constructor(
        protected $http,
        protected $q,
        protected $timeout,
        protected Util: Util
    ) {
        super($http, $q);
        this.baseUrl += 'teacher/';
    }

    profile() {
        return this.get()
            .then(q => q.value())
    }
}
