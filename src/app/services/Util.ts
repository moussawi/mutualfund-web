export class Util {

    private currentLanguage = 'en';
    private colorsCache = {};
    
    static $name = 'Util';
    static $inject = [];
    
    constructor() {}

    private applyI18n(context, level, paths) {

        if (_.isArray(context)) {
            context.forEach(x => {
                this.applyI18n(x, level, paths);
            });
            return;
        }

        if (level < paths.length - 1) {
            this.applyI18n(context[paths[level + 1]], level + 1, paths);
        } else {
            if (_.isUndefined(context) || _.isUndefined(context.i18n)) {
                console.warn(context, "context does not contain i18n");
                return;
            }
            _.forEach(context.i18n[this.currentLanguage], function(value, key: string) {
                if (!(key === 'entityId' || key === 'language')) {
                    context[key + '_i18n'] = value;
                }
            });
        }
    }


    public withI18n(q, path) {
        path = _(path.split('.')).filter(x => x.length).unshift('').value();
        return q.forEach((x: any) => {
            this.applyI18n(x, 0, path);
        }, this);
    }


    public withColors(q, propertyFn) {
        return q.forEach((x: any) => {
            var property = propertyFn(x);

            if (!this.colorsCache[property]) {
                this.colorsCache[property] = Please.make_color()[0];
            }

            x.color = this.colorsCache[property];
        });
    }

    public withInitials(q, propertyFn, count = 2) {
        return q.forEach((x: any) => {
            var property = propertyFn(x);
            var initial = _(property.split(/\s+/))
                .map(x => x[0])
                .take(count)
                .value();
            x.initialCount = initial.length;
            x.initial = initial.join('');
        })
    }
}