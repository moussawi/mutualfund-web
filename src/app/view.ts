interface Window {
	Config: {
		baseUrl: string,
		noCache: boolean
	}
}

var Config = window['Config'] || { baseUrl: '/' };

/**
 * View locator
 * 
 */
var View = function(name:string, noCache = null){
	
	if(noCache === null){
		noCache = window['Config']['noCache'];
	}
	
	if(name.indexOf('.html') === -1){
		name += '.html';	
	}
	
	if(noCache){
		name += '?v' + (new Date).getMilliseconds();
	}
	
	
	return 'views/' + name; 
}

export default View;  