System.config({
    baseURL: '.',
    defaultJSExtensions: true,
    map: {
        lib: './src/lib'
    },
    meta: {
        'lib/*': { format: 'global' }
    }
});